
// Query Operators and Field projectionin MongoDB
	// oppose to exact match
	// allows more flexible



db.users.insertOne({
	 firstName:"Bill",
            lastName: "Gates",
            age: 65,
            contact: {
                phone: "12345678",
                email: "bill@gmail.com",
            },
            courses:["PHP", "Laravel", "HTML"],
            department: "Operations"
});


// [SECTION] Comparison Query Operators

	// $gt/$gte operator (>,>=)
	/*
		-allows to find document that have field values greater than or equal to specified value

		Syntax:
			db.collectName.find({field: {$gt:value}})
			db.collectName.find({field: {$gt:value}})
	*/


	db.users.find({age: {$gt:65}})  //>
	db.users.find({age: {$gte:65}})  //>=




	// $lt/$lte operator (<,<=)
	/*
		-allows to find document that have field values less than or equal to specified value

		Syntax:
			db.collectName.find({field: {$lt:value}})
			db.collectName.find({field: {$lt:value}})
	*/

	db.users.find({age: {$lt:65}})  //<
	db.users.find({age: {$lte:65}})  //<=



	// $ne operator 
	/*
		-allows to find document that have field values NOT equal to specified value

		Syntax:
			db.collectName.find({field: {$ne:value}})
			db.collectName.find({field: {$ne:value}})
	*/

	db.users.find({age: {$ne:82}})




	// $in operator
		/*
		-allows to find document with specific match criteria of one field using different value.

		Syntax:
			db.collectName.find({field: {$in: [valueA, valueB]}})

	*/

	db.users.find({lastName: {$in: ["Hawking","Doe"]}})
	db.users.find({courses: {$in:["HTML","React"]}})





// [SECTION] Logical Operators
	
	// $or operator
		/*
			-allows to find documents that match a single criteria from multiple provided search criteria

			Syntax:
				db.collectionName.find( {$or: [{fieldA:valueA},{fieldB:valueB]})
		*/

		db.users.find ({
			$or:[
				{firstName: "Neil"},
				{age:25}
				]
			})


	// With comparison operator
		db.users.find ({
			$or:[
				{firstName: "Neil"},
				{age: {$gt:30}}
				]
			})



	// $and
		/*
			-allows us to find documents matching multiple criteria in a single field

			Syntax:
				db.collectionName.find( {$and: [{fieldA:valueA}, {fieldB:valueB]});

		*/
		db.users.find({
			$and:[
					{age:{$ne:82}},
					{age:{$ne:76}},
				]
		})

// Mini Activity
		Condition is 

		db.users.find({
			$and:[
					{courses: {$in: ["Laravel","React"]}},
					{age:{$lt:80}},
				]
		})



// [SECTION] Field Projection
	// To help with the readability of the values returned, we can include/exclude fields from the retrieve results.

	// Inclusion
		/*
			-allows us to include/add specific field only when retrieving documents.
			-the value provided is 1 to denote that the field is being included.

			Syntax:
				db.collectionName.find({criteria},{field:1})
		*/


		db.users.find(
				{
					firstName: "Jane"},
				{
					firstName:1,
					lastName:1,
					contact:1
				}
			)


	// Exclusion
		/*
			-allows us to exclude/add specific field only when retrieving documents.
			-the value provided is 0 to denote that the field is being excluded.

			Syntax:
				db.collectionName.find({criteria},{field:0})
		*/


		db.users.find(
		{
			firstName: "Jane"
		},
		{
			_id:0,
			contact:0,
			department:0
		}
			)



// Mini Activity

		db.users.find(
		{
			lastName: "Doe"
		},
		{
			_id:0,
			firstName:1,
			lastName:1,
			contact:1
		}
			)


		// Suppressing the ID Field
			// when using field projection, field inclusion and exlusion may not be used at the same time.
			// excluding the "_id" field is the only exception to this rule.
			// Syntax: db.collectionName.find({criteria},{field:1, _id:0})



		// Return a specific field in Embedded Documents.


		db.users.find(
			{firstName:"Jane"},
			{
				firstName:1,
				lastName:1,
				"contact.phone":1
			}

			);

		// Exclude
		db.users.find(
		{firstName:"Jane"},
		{"contact.phone":0}

		)

		// Project Specific Elements in the returned array
			// The $slice operator allows us to retrieve element that matches the criteria.
			// shows the element


			/*
			Syntax1:
				db.collection.find({criteria},arrayField: {$slice:count});

			*/

		db.users.find(
			{firstName: "Jane"},
			{courses:{$slice:2}}

			)

			/*
			Syntax2:
				db.collection.find({criteria},arrayField: {$slice:[index,count/number]]});

			*/



//[SECTION] Evaluation Query Operator


		// db.users.find({firstName:"jane"})

		// $regex operator
			// allows us to find documents that match a specific string pattern using "regular expression"/"regex"

		/*
			Syntax:
			db.collectionName.find({field: {$regex:"pattern", $options: "optionValue"}})
		*/

		// case sensitive query

		db.users.find({firstName: {$regex: "ne"}});

		// case insensitive query
		db.users.find({firstName: {$regex: "ne",$options:"$i"} });